import { browser, by, element } from 'protractor';

export class AppPage {
  navigateTo() {
    return browser.get('/');
  }

  getTitleText() {
    return element(by.css('app-root h1')).getText();
  }

  getMaleText() {
    return element(by.css('#male')).getText();
  }

  getFemaleText() {
    return element(by.css('#female')).getText();
  }

  checkMaleCatDisplay() {
    return element(by.css('#male-0')).isDisplayed();
  }

  checkFemaleCatDisplay() {
    return element(by.css('#female-0')).isDisplayed();
  }
}
