import { AppPage } from './app.po';

describe('workspace-project App', () => {
  let page: AppPage;

  beforeEach(() => {
    page = new AppPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getTitleText()).toEqual('Pets Application');
  });

  it('should display welcome message for Male Cats', () => {
    page.navigateTo();
    expect(page.getMaleText()).toEqual('Male Cats');
  });

  it('should display welcome message for Female Cats', () => {
    page.navigateTo();
    expect(page.getFemaleText()).toEqual('Female Cats');
  });

  it('should display Female Cats', () => {
    page.navigateTo();
    expect(page.checkFemaleCatDisplay()).toBeTruthy();
  });

  it('should display Male Cats', () => {
    page.navigateTo();
    expect(page.checkMaleCatDisplay()).toBeTruthy();
  });
});
