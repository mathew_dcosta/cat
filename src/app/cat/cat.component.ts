// Angular Imports
import { Component, OnInit } from '@angular/core';

// Custom Imports
import { PetService } from '../services/pet.service';
import { People } from '../model/people.model';
import * as COMMON_CONSTANTS from '../common/common.constants';

@Component({
  selector: 'app-cat',
  templateUrl: './cat.component.html',
  styleUrls: ['./cat.component.scss']
})
export class CatComponent implements OnInit {

  // variables
  people: People[];
  maleCats: People[];
  femaleCats: People[];
  catNames: string[];
  isError: boolean;

  errorMessage: string = COMMON_CONSTANTS.ERROR_MESSAGE;
  noDataFound: string = COMMON_CONSTANTS.NO_DATA;

  /**
   * Constructor
   */
  constructor(
    private petService: PetService
  ) { }

  /**
   * On Init of component
   */
  ngOnInit() {
    this.isError = false;
    this.getCats();
  }

  /**
   * Calls the pet service
   */
  getCats() {
    this.petService.getPets().subscribe(cats => {
      if (cats) {
        this.people = cats;
        this.maleCats = this.sortCatNames(COMMON_CONSTANTS.MALE.toLowerCase());
        this.femaleCats = this.sortCatNames(COMMON_CONSTANTS.FEMALE.toLowerCase());
      }
    },
      (error) => {
        // change the error flag to show error message
        this.isError = true;
      });
  }

  /**
   * Method to sort the cats by gender
   * @param gender Gender for sort
   */
  sortCatNames(gender: string) {
    return [].concat.apply([], (this.people
      .filter(p => p.gender.toLowerCase() === gender))
      .map(o => o.pets))
      .filter(m => m && m.type.toLowerCase() === COMMON_CONSTANTS.CAT.toLowerCase())
      .map(n => n.name)
      .sort();
  }

}
