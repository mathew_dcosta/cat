import { async, ComponentFixture, TestBed, getTestBed } from '@angular/core/testing';
import { HttpClient, HttpHandler } from '@angular/common/http';
import { CatComponent } from './cat.component';
import { PeopleMockService } from '../mock/people-mock';
import { PetService } from '../services/pet.service';
import { HttpClientModule } from '@angular/common/http';
import { Pets } from '../model/people.model';
import { of, throwError } from 'rxjs';
import { By } from '@angular/platform-browser';
import * as COMMON_CONSTANTS from '../common/common.constants';

describe('CatComponent', () => {
  let component: CatComponent;
  let fixture: ComponentFixture<CatComponent>;
  let petService: PetService;

  let mockService: PeopleMockService;
  let injector: TestBed;
  let debugElement;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientModule],
      declarations: [CatComponent],
      providers: [

      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CatComponent);
    component = fixture.componentInstance;
    injector = getTestBed();
    mockService = injector.get(PeopleMockService);
    petService = fixture.debugElement.injector.get(PetService);
    debugElement = fixture.debugElement;

  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call getPets and return a single list of pets', () => {
    const response: Pets[] = [];
    spyOn(petService, 'getPets').and.returnValue(of(mockService.peopleSingleResponse));
    component.getCats();
    expect(component.people).toEqual(mockService.peopleSingleResponse);
  });

  it('should call getPets and return an empty list of pets', () => {
    const response: Pets[] = [];
    spyOn(petService, 'getPets').and.returnValue(of(mockService.peopleEmptyResponse));
    component.getCats();
    expect(component.people.length).toBe(0);
  });

  it('should call getPets and return the list of pets', () => {
    const response: Pets[] = [];
    spyOn(petService, 'getPets').and.returnValue(of(mockService.peopleResponse));
    component.getCats();
    expect(component.people.length).toBe(6);
  });

  it('should call getPets and have the list items in the Male section in ascending order', () => {
    spyOn(petService, 'getPets').and.returnValue(of(mockService.peopleResponse));
    component.getCats();
    expect(component.people.length).toBe(6);
    fixture.detectChanges();
    fixture.whenStable().then(() => {
      expect(debugElement.query(By.css('#male-0')).nativeElement.innerText).
        toBe('Garfield');
      expect(debugElement.query(By.css('#male-1')).nativeElement.innerText).
        toBe('Jim');
      expect(debugElement.query(By.css('#male-2')).nativeElement.innerText).
        toBe('Max');
      expect(debugElement.query(By.css('#male-3')).nativeElement.innerText).
        toBe('Tom');
    });
  });

  it('should call getPets and have the list items in the female section in ascending order', () => {
    spyOn(petService, 'getPets').and.returnValue(of(mockService.peopleResponse));
    component.getCats();
    expect(component.people.length).toBe(6);
    fixture.detectChanges();
    fixture.whenStable().then(() => {
      expect(debugElement.query(By.css('#female-0')).nativeElement.innerText).
        toBe('Garfield');
      expect(debugElement.query(By.css('#female-1')).nativeElement.innerText).
        toBe('Simba');
      expect(debugElement.query(By.css('#female-2')).nativeElement.innerText).
        toBe('Tabby');
    });
  });

  it('should call getPets and check if the female list is not in ascending order', () => {
    spyOn(petService, 'getPets').and.returnValue(of(mockService.peopleResponse));
    component.getCats();
    expect(component.people.length).toBe(6);
    fixture.detectChanges();
    fixture.whenStable().then(() => {
      expect(debugElement.query(By.css('#female-0')).nativeElement.innerText).not
        .toBe('Simba');
    });
  });

  it('should call getPets and check if the male list is not in ascending order', () => {
    spyOn(petService, 'getPets').and.returnValue(of(mockService.peopleResponse));
    component.getCats();
    expect(component.people.length).toBe(6);
    fixture.detectChanges();
    fixture.whenStable().then(() => {
      expect(debugElement.query(By.css('#male-2')).nativeElement.innerText).not
        .toBe('Garfield');
    });
  });

  it('should call getPets and errors out', () => {
    spyOn(petService, 'getPets').and.returnValue(throwError({ status: 404 }));
    component.getCats();
    fixture.detectChanges();
    fixture.whenStable().then(() => {
      expect(debugElement.query(By.css('.error')).nativeElement.innerText)
        .toBe(COMMON_CONSTANTS.ERROR_MESSAGE);
    });
  });

  it('should call getPets and show warning when empty response is returned', () => {
    const response: Pets[] = [];
    spyOn(petService, 'getPets').and.returnValue(of(mockService.peopleEmptyResponse));
    component.getCats();
    expect(component.people.length).toBe(0);
    fixture.detectChanges();
    fixture.whenStable().then(() => {
      expect(debugElement.query(By.css('.warn')).nativeElement.innerText)
        .toBe(COMMON_CONSTANTS.ERROR_MESSAGE);
    });
  });
});
