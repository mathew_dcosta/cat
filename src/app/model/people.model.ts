// Model for People
export interface People {
    name: string;
    gender: string;
    age: number;
    pets: Pets[];
}

// Model for pets
export interface Pets {
    name: string;
    type: string;
}
