export const ERROR_MESSAGE = 'This doesn\'t look good! Please try after sometime';
export const MALE = 'male';
export const FEMALE = 'female';
export const CAT = 'cat';
export const NO_DATA = 'No data found!';
