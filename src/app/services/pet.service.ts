// Angular Imports
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

// Custom Imports
import { People } from '../model/people.model';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class PetService {

  // Get the baseUrl variable from environment
  baseUrl = environment.baseUrl;

  /**
   * Constructor
   */
  constructor(private http: HttpClient) { }

  /**
   * Calls the people service
   */
  getPets(): Observable<People[]> {
    return this.http.get<People[]>(`${this.baseUrl}/people.json`);
  }
}
