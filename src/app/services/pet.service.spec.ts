import { TestBed, getTestBed } from '@angular/core/testing';
import { HttpClientModule } from '@angular/common/http';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { PeopleMockService } from '../mock/people-mock';
import { PetService } from './pet.service';

describe('PetService', () => {
  let injector: TestBed;
  let petService: PetService;
  let httpMock: HttpTestingController;
  let mockService: PeopleMockService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientModule, HttpClientTestingModule],
      providers: [PetService, PeopleMockService]
    });
    injector = getTestBed();
    petService = injector.get(PetService);
    httpMock = injector.get(HttpTestingController);
    mockService = injector.get(PeopleMockService);
  });

  it('should be created', () => {
    const service: PetService = TestBed.get(PetService);
    expect(service).toBeTruthy();
  });

  it('should return cat names in sorted order', () => {
    petService.getPets().subscribe((pets) => {
      expect(pets.length).toBe(6);
      expect(pets).toEqual(mockService.peopleResponse);
    });
    const httpRequest = httpMock.expectOne(`${petService.baseUrl}/people.json`);
    expect(httpRequest.request.method).toBe('GET');
    httpRequest.flush(mockService.peopleResponse);
    httpMock.verify();
  });
});
