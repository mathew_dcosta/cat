# CatNames

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 7.2.1.

# Introduction

The project is composed of the following components:
1. Cat component:
     This component has the logic to sort the cats by gender

Appropriate unit tests are in place and a few e2e test cases have also been included. The code coverage is 100%. 

To run the application, kindly clone the repository and run  `npm install` on the root directory.

## Development server

Run `ng run start` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.


## Running unit tests

Run `npn run test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `npm run e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Code Coverage

Run `npm run test --code-coverage` to get the code coverage report. 
